var app_api = "http://slabs.cc/BGCA/api/";
var clubs_api = "http://bgca.cartodb.com/api/v2/sql";

app.factory('Club', function($http){
	var club = {
		query: function(params){
			return $http.get(clubs_api, {
				params: params
			});
		}
		
	};
	return club;
});

app.factory('Activity', function($http){
	var activity = {
		query: function(params){
			return $http.get(app_api+"Activity", {
				params: params
			});
		},
		random: function(params){
			return $http.get(app_api+"Activity/Random", {
				params: params
			});
		},
		like: function(userid, activityid){
			var params = {
				activityid: activityid,
				userid: userid
			}
			return $http.post(app_api+"ActivityLike", params);
		}
		
	};
	return activity;
});

app.factory('Category', function($resource){
	return $resource('mock/Category.json');
});

app.factory('Cost', function($resource){
	return $resource('mock/Cost.json');
});

app.factory('Ethnicity', function($resource){
	return $resource('mock/Ethnicity.json');
});

app.factory('Genre', function($resource){
	return $resource('mock/Genre.json');
});

/*app.factory('OrgMetaData', function($resource){
	return $resource('mock/OrgMetaData.json');
});*/

app.factory('OrgMetaData', function($http){
	var data = {
		query: function(params){
			return $http.get(app_api+"OrgMetaData", {
				params: params
			});
		},
		get: function(orgid){
			return $http.get(app_api+"OrgMetaData/"+orgid);
		}
		
	};
	return data;
});

app.factory('Story', function($http){
	var story = {
		query: function(params){
			return $http.get(app_api+"Story", {
				params: params
			});
		}
		
	};
	return story;
});

app.factory('User', function($http){
	var user = {
		query: function(params){
			return $http.get(app_api+"User", {
				params: params
			});
		},
		create: function(data){
			return $http.post(app_api+"User", data);
		}
		
	};
	return user;
});
