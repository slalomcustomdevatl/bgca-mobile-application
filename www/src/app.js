'use strict';

var app = angular.module('BGCAApp', [
  'ui.router',
  'mobile-angular-ui',
  'infinite-scroll',
  'ngResource'
])
  .config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
  $stateProvider
  	.state('app', {
      templateUrl: "partials/app.html",
      abstract: true
    })
    .state('home', {
      templateUrl: "partials/home.html",
      abstract: true
    })
    .state('home.login', {
      url: "/login",
      templateUrl: "partials/login.html",
      controller: "LoginCtrl"
    })
    .state('home.main', {
      url: "/",
      templateUrl: "partials/main.html",
      controller: "MainCtrl"
    })
    .state('app.stories_search', {
      url: "/stories/search",
      templateUrl: "partials/stories_search.html",
      resolve: {

	        Genre: 'Genre',
	        Ethnicity: 'Ethnicity',
	        genres: function(Genre){
	            return Genre.query();
	        },
	        ethnicities: function(Ethnicity){
	            return Ethnicity.query();
	        } 
	  },
      controller: "StoriesSearchCtrl",
    })
    .state('app.stories', {
      url: "/stories/list",
      templateUrl: "partials/stories.html",
      controller: "StoriesCtrl"
    })
    .state('app.story', {
      url: "/stories/:id",
      templateUrl: "partials/story.html",
      controller: "StoryCtrl"
    })
     .state('app.story_add', {
      url: "/stories/add",
      templateUrl: "partials/story_add.html",
      controller: "StoryAddCtrl"
    })
    .state('app.clubs_search', {
      url: "/clubs/search",
      templateUrl: "partials/club_search.html",
      controller: "ClubsSearchCtrl"
    })
    .state('app.clubs', {
      url: "/clubs/list",
      templateUrl: "partials/clubs.html",
      resolve: {
	        OrgMetaData: 'OrgMetaData',
	        metaData: function(OrgMetaData){
	            return OrgMetaData.query();
	        }
	  },
      controller: "ClubsCtrl"
    })
    .state('app.club', {
      url: "/clubs/:id",
      templateUrl: "partials/club.html",
      controller: "ClubCtrl"
    })
    .state('app.activities_search', {
      url: "/activities/search",
      templateUrl: "partials/activities_search.html",
      resolve: {

	        Cost: 'Cost',
	        Category: 'Category',
	        costs: function(Cost){
	            return Cost.query();
	        },
	        categories: function(Category){
	            return Category.query();
	        } 
	  },
      controller: "ActivitiesSearchCtrl"
    })
    .state('app.activities', {
      url: "/activities/list",
      templateUrl: "partials/activities.html",
      controller: "ActivitiesCtrl"
    })
    .state('app.activity_add', {
      url: "/activities/add",
      templateUrl: "partials/activity_add.html",
      resolve: {

	        Cost: 'Cost',
	        Category: 'Category',
	        costs: function(Cost){
	            return Cost.query();
	        },
	        categories: function(Category){
	            return Category.query();
	        } 
	  },
      controller: "ActivityAddCtrl"
    })
    .state('app.activity', {
      url: "/activity/:id",
      templateUrl: "partials/activity.html",
      controller: "ActivityCtrl"
    })
    .state('home.logout', {
      url: "/logout",
      templateUrl: "partials/main.html",
      controller: "LogoutCtrl"
    });
    
    $urlRouterProvider.otherwise("/login");
    
    /*$httpProvider.defaults.useXDomain = true;
    delete $httpProvider.defaults.headers.common['X-Requested-With'];*/
  });