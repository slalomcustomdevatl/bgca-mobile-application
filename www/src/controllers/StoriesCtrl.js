app.controller("StoriesSearchCtrl", ['$scope', '$state', '$window', 'StoryService', 'UserService', 'genres', 'ethnicities', 
function($scope, $state, $window, StoryService, UserService, genres, ethnicities){
	
	UserService.checkSession();
	
	$scope.isGuest = UserService.isGuest;
	$scope.cultures = ethnicities;
	$scope.genres = genres;
	
	$scope.viewAllSearch = function(){
		var filters = {};
		StoryService.setSearchParams("View All", filters);
		$state.go('app.stories');
	};
	
	$scope.search = function(){
		var costs = [];
		if($scope.cost1) { costs.push(1); }
		if($scope.cost2) { costs.push(2); }
		if($scope.cost3) { costs.push(3); }
		if($scope.cost4) { costs.push(4); }
		if($scope.cost5) { costs.push(5); }

		var filters = {
			'$orderby': "likes desc",
			'keywords': $scope.keywords,
			'category': $scope.category,
			'costs': costs,
			'sponsored': $scope.sponsored,
			'age': $('#age_slider').data('slider').getValue(),
			'startDate': $('#search_start_date').val(),
			'endDate': $('#search_end_date').val()
		};
		console.log(filters);
		ActivityService.setSearchParams("Advanced Search", filters);
		$state.go('app.activities');
	};

	$scope.back = function() {
    	$window.history.back();
    };
}]);

app.controller("StoriesCtrl", ['$scope', '$state', '$window', 'StoryService', 'Story', 
function($scope, $state, $window, StoryService, Story){
	$scope.noResults = false;
	$scope.scrollBusy = false;
	$scope.stories = StoryService.stories;
	$scope.filteredby = StoryService.filteredby;
	$scope.filters = StoryService.filters;
	
	$scope.loadStories = function() {
		
        $scope.scrollBusy = true;
        $scope.filters['$skip'] = $scope.stories.length;
        
        Story.query($scope.filters).then(function(stories) {
        	
            if (stories.data.length > 0) {
            	
                $scope.stories = $scope.stories.concat(stories.data);
                $scope.scrollBusy = false;
            } 
            else {
                $scope.noResults = true;
            }
        });
    };
    
    $scope.showStory = function(storyId){
    	var story = null;
    	var count = 0;
    	
    	for(var i = 0; i < $scope.stories.length; i++){
    		
    		if($scope.stories[i].id == storyId){
    			story = $scope.stories[i];
    			break; 
    		}		
    	}
    	
    	if(story != null){
    		StoryService.setStoriesParams($scope.stories);
    		StoryService.setstoryParams(story);
    		$state.go('app.story');
    	}

    }

    $scope.back = function() {
    	$window.history.back();
    };

    $scope.viewAddStory = function(){
		$state.go('app.story_add');
	};
    
    if($scope.stories.length == 0){
    	$scope.loadStories();	
    }
}]);

app.controller("StoryCtrl", ['$scope', function($scope){
	
}]);
