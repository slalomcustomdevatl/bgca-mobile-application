app.controller("ClubsSearchCtrl", ['$scope', '$state', 'UserService', 'ClubService', function($scope, $state, UserService, ClubService){
	UserService.checkSession();
    $scope.zipInput = '';
    $scope.geocoder = new google.maps.Geocoder();
    $scope.zipCodeSearch = function () {
        
       $scope.geocoder.geocode({
            'address': $scope.zipInput
        }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                console.log(results[0].geometry.location);
                var position = { 'coords': { 'latitude': results[0].geometry.location.k, 'longitude': results[0].geometry.location.B } };
                var filters = {
                    q: ClubService.generatePositionQuery(position, 20)
                }
                ClubService.setPosition(position);
                ClubService.setSearchParams('Clubs Near ' + $scope.zipInput, filters);
                $state.go('app.clubs');
            } else {
                alert("Geocode was not successful for the following reason: "
                      + status);
            }
        });
    };

	$scope.nearMeSearch = function(){
		navigator.geolocation.getCurrentPosition(function(position){
			console.log(position);
			var filters = {
				q: ClubService.generatePositionQuery(position, 20)
			};
			ClubService.setPosition(position);
			ClubService.setSearchParams("Near Me", filters);
			$state.go('app.clubs');	
		},
		function(error){
			
		});
		
	};
	
	$scope.performSearch = function(){
		var geocoder = new google.maps.Geocoder();
		geocoder.geocode({address: $scope.query }, function(results){
			if(results.length > 0)
			{
				var location = results[0];
				var position = {
					coords: {
						latitude: location.geometry.location.k,
						longitude: location.geometry.location.B
					}
				}
				var filters = {
					q: ClubService.generatePositionQuery(position, 20)
				};
				ClubService.setSearchParams($scope.query, filters);
				$state.go('app.clubs');	
			}	
		});
	};
}]);

app.controller("ClubsCtrl", ['$scope', 'OrgMetaData', 'Club', 'ClubService', function($scope, OrgMeta, Club, ClubService){
	$scope.filters = ClubService.filters;
	$scope.filteredby = ClubService.filteredby;
    $scope.clubView = 'list';
	$scope.clubs = ClubService.clubs;
    
	$scope.loading = false;
	
	$scope.loadClubs = function() {
		$scope.loading = true;
        Club.query($scope.filters).then(function(clubs) {
            if (clubs.data.rows.length > 0) {
                $scope.clubs = clubs.data.rows;
            } 
            else {
                $scope.noResults = true;
            }
        }).finally(function(){
        	$scope.loading = false;
        });
    };
    
    $scope.viewMap = function() {
        $scope.clubView = 'map';
        $('#club-map').height($('.page').height()-90);
        var mapOptions = {
            //center: { lat: 33.801905, lng: -84.387355},
            center: { lat: ClubService.getPosition().coords.latitude, lng: ClubService.getPosition().coords.longitude },
            zoom: 10,
            disableDefaultUI: true
        };
        var map = new google.maps.Map(document.getElementById('club-map'), mapOptions);

        for(var i=0; i<$scope.clubs.length; i++) {
            var club = $scope.clubs[i];
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(club.latitude,club.longitude),
                map: map,
                title: club.name
            });
        }
        
    };

    $scope.viewList = function() {
        $scope.clubView = 'list';
    };

    $scope.loadClubs();
}]);

app.controller("ClubCtrl", ['$scope', function($scope){
	
}]);

