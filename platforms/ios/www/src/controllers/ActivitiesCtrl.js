app.controller("ActivitiesSearchCtrl", ['$scope', '$state', '$http', '$window', 'ActivityService', 'UserService', 'costs', 'categories', 'Cost',
function($scope, $state, $http, $window, ActivityService, UserService, costs, categories, Cost){

	UserService.checkSession();
	$scope.searchtype = 'basic';
	$scope.isGuest = UserService.isGuest;
	$scope.categories = categories;
	$scope.costs = costs;
	$scope.sponsored = "Both";

	$scope.favoritesSearch = function(){
		var filters = {
			bygeo: false
		};
		ActivityService.setSearchParams("My Favorites", filters);
		$state.go('app.activities');
	};
	
	$scope.randomSearch = function(){
		var filters = {
			bygeo: false
		};
		ActivityService.setSearchParams("Random", filters);
		$state.go('app.activities');
	};
	
	$scope.nearMeSearch = function(){
		navigator.geolocation.getCurrentPosition(function(position){
			var filters = {
				bygeo: true,
				lat: position.coords.latitude,
				lng: position.coords.longitude
			};
			ActivityService.setSearchParams("Near Me", filters);
			$state.go('app.activities');
		},
		function(error){
			
		});
	};
	
	$scope.popularSearch = function(){
		var filters = {
			bygeo: false,
			'$orderby': "likes desc"	
		};
		ActivityService.setSearchParams("Most Popular", filters);
		$state.go('app.activities');
	};
	
	$scope.viewAllSearch = function(){
		var filters = {
			bygeo: false	
		};
		ActivityService.setSearchParams("View All", filters);
		$state.go('app.activities');
	};
	
	$scope.advancedSearch = function(){
		var costs = [];
		if($scope.cost1) { costs.push(1); }
		if($scope.cost2) { costs.push(2); }
		if($scope.cost3) { costs.push(3); }
		if($scope.cost4) { costs.push(4); }
		if($scope.cost5) { costs.push(5); }

		var filters = {
			'$orderby': "likes desc",
			'keywords': $scope.keywords,
			'category': $scope.category,
			'costs': costs,
			'sponsored': $scope.sponsored,
			'age': $('#age_slider').data('slider').getValue(),
			'startDate': $('#search_start_date').val(),
			'endDate': $('#search_end_date').val()
		};
		console.log(filters);
		ActivityService.setSearchParams("Advanced Search", filters);
		$state.go('app.activities');
	};
	
	$scope.back = function() {
    	$window.history.back();
    };

}]);

var parseTSQLDate = function(str) {
	if(!str || str.split('T').length < 1) {
		return;
	}
	var d = str.split('T')[0].split('-');
	var t = str.split('T')[1].split('.')[0].split(':')
	var date = new Date(d[0], d[1]-1, d[2], t[0], t[1], t[2]);
	return date;
};

app.controller("ActivitiesCtrl", ['$scope', '$state', '$window', 'Activity', 'ActivityService', 'UserService',
function($scope, $state, $window, Activity, ActivityService, UserService){
	$scope.noResults = false;
	$scope.scrollBusy = false;
	$scope.isGuest = UserService.isGuest;
	$scope.activities = ActivityService.activities;
	$scope.filteredby = ActivityService.filteredby;
	$scope.filters = ActivityService.filters;
	
	$scope.loadActivities = function() {
        $scope.scrollBusy = true;
        $scope.filters['$skip'] = $scope.activities.length;
        
        var callback = function(activities) {
            if (activities.data.length > 0) {
            	
                $scope.activities = $scope.activities.concat(activities.data);
                $scope.activities.forEach(function(activity) {
                	activity.start = parseTSQLDate(activity.activitydate);
                	activity.end = parseTSQLDate(activity.activitydateend);
                });
                $scope.scrollBusy = false;
            } 
            else {
                $scope.noResults = true;
            }
        };
        
        if($scope.filteredby == "Random")
        {
        	Activity.random($scope.filters).then(callback);
        }
        else
        {
        	Activity.query($scope.filters).then(callback);	
        }
        
    };

    $scope.favorite = function(activity) {
    	if(UserService.id) {
    		Activity.like(UserService.id, activity.id);
    	}
    	// animate
    	$("#activity_fav_"+activity.id).animate({opacity: 1});
    };
    
    $scope.showActivity = function(activityId){
    	var activity = null;
    	var count = 0;
    	
    	for(var i = 0; i < $scope.activities.length; i++){
    		
    		if($scope.activities[i].id == activityId){
    			activity = $scope.activities[i];
    			break; 
    		}		
    	}
    	
    	if(activity != null){
    		ActivityService.setActivitiesParams($scope.activities);
    		ActivityService.setActivityParams(activity);
    		$state.go('app.activity');
    	}

    }

    $scope.back = function() {
    	$window.history.back();
    };

    $scope.viewAddActivity = function(){
    	ActivityService.setActivitiesParams($scope.activities);
		$state.go('app.activity_add');
	};
    
    $scope.searchActivities = function() {
    	$state.go('app.activities_search');
    };
    
    if($scope.activities.length == 0){
    	$scope.loadActivities();	
    }
}]);

app.controller("ActivityCtrl", ['$scope', '$window', 'ActivityService', function($scope, $window, ActivityService){
	$scope.activity = ActivityService.activity;

	$scope.activity.start = parseTSQLDate($scope.activity.activitydate);
	$scope.activity.end = parseTSQLDate($scope.activity.activitydateend);

	$scope.back = function() {
    	$window.history.back();
    };
}]);

app.controller("ActivityAddCtrl", ['$scope', '$state', '$window', 'ActivityService', 'costs', 'categories',
 function($scope, $state, $window, ActivityService, costs, categories){
	$scope.categories = categories;
	$scope.costs = costs;

	$scope.back = function() {
    	$window.history.back();
    };
}]);

