app.controller("LoginCtrl", ['$scope', 'UserService', 'User',
	function($scope, UserService, User){
	$scope.googleplusLogin = function() {
		var additionalParams = {
     		'callback': $scope.googlepluscallback
     	};
		gapi.auth.signIn(additionalParams);
	};

	$scope.facebookLogin = function() {

	};

	$scope.twitterLogin = function() {

	};

	$scope.guestLogin = function(){
		UserService.loginGuest();	
	};
	
	$scope.googleusercallback = function googleusercallback(userInfo) {
		UserService.name = userInfo.displayName;
		UserService.email = userInfo.emails[0].value;

		var userCallback = function(ret) {
			UserService.id = ret.data.id
			UserService.loginUser();
		};

		var userCreated = function(ret) {
			UserService.id = ret.data.id;
			UserService.loginUser();	
		};

		var userError = function(ret) {
			//createUser if not found
			data = {
				email: UserService.email,
				name: UserService.name
			};
			if (ret.status === 404) {
				User.create(data).then(userCreated);				
			}
		};

		//create or retrieve id
		var filters = {
			id: UserService.email
		};
		User.query(filters).then(userCallback, userError);
		//UserService.loginUser();
	}

	$scope.googlepluscallback = function googlepluscallback(authResult) {
		if(authResult['access_token']) {
            gapi.client.request(
        	{
	            'path':'/plus/v1/people/me',
	            'method':'GET',
	            'callback': $scope.googleusercallback
	        });
        } 
	}


}]);

app.controller("LogoutCtrl", ['$scope', 'UserService', function($scope, UserService){
	UserService.logout();
	$scope.googleplusLogout = function() {
		gapi.auth.signOut();
	}
	
}]);