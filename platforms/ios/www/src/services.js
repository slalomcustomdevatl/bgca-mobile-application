app.factory('ActivityService', function($rootScope){
	var activityService = {
		filters : {},
		filteredby: '',
		activities: [],
		activity: null,
		setSearchParams: function(filteredby, filters){
			this.filters = filters;
			this.filteredby = filteredby;
		},
		setActivitiesParams: function(activities){
			this.activities = activities;
		},
		setActivityParams: function(activity){
			this.activity = activity;
		}
	};

	return activityService;
});

app.factory('StoryService', function($rootScope){
	var storyService = {
		filters : {},
		filteredby: '',
		stories: [],
		story: null,
		setSearchParams: function(filteredby, filters){
			this.filters = filters;
			this.filteredby = filteredby;
		},
		setStoriesParams: function(stories){
			this.stories = stories;
		},
		setStoryParams: function(story){
			this.story = story;
		}
	};

	return storyService;
});

app.factory('ClubService', function($rootScope){
	var clubService = {
	    filters: {},
	    position: {},
		filteredby: 'Atlanta, GA',
		clubs: [],
		club: null,
		columns: [
			"name",
			"address_1",
			"address_2",
			"city",
			"state",
			"zip_code",
			"phone_number",
			"url",
			"insight_id",
			"organization_id",
			"global_id",
			"category",
			"status",
			"county",
			"address_3",
			"address_4",
			"country",
			"fax_number",
			"square_footage",
			"capacity",
			"promotional_date",
			"impact_assessment_year",
			"the_geom",
			"the_geom_webmercator",
			"region",
			"primary_site",
			"military_base_type",
			"membership_date",
			"impact_assessment_approved_date"
		],
		verbatimColumns: [
			"st_y(the_geom) as latitude",
			"st_x(the_geom) as longitude",
		],
		partialQuery: " FROM (SELECT * FROM bgca_sites_2014_02_11 WHERE status IN ('Active - Full Membership')) as _cartodbjs_alias ",
		setSearchParams: function(filteredby, filters){
			this.filters = filters;
			this.filteredby = filteredby;
		},
		setClubParams: function(club){
			this.club = club;
		},
		setClubsParams: function(clubs){
			this.clubs = clubs;
		},
		generatePositionQuery: function(position, limit){
			var query = "SELECT \""+this.columns[0]+"\"";
			for(var i = 1; i < this.columns.length; i++)
			{
				query = query+",\""+this.columns[i]+"\"";
			}
			
			for(var i = 0; i < this.verbatimColumns.length; i++)
			{
				query = query+","+this.verbatimColumns[i];
			}
			
			query = query + this.partialQuery + "ORDER BY the_geom <-> ST_SetSRID(ST_MakePoint("+position.coords.longitude+", "+position.coords.latitude+"), 4326) ASC LIMIT "+limit;
			
			return query;
		},
		setPosition: function (pos) {
		    this.position = pos;
		},
		getPosition: function () {
		    return this.position;
		}
		
	};

	return clubService;
});

app.factory('LocationService', function($rootScope, $state, $http){
	var locationService = {
		latitude: 0,
		longitude: 0,
		fetchGeoPoint: function(location){
			return $http.get
		}
	};
});
app.factory('UserService', function($rootScope, $state){
	var userService = {
		isLoggedIn: false,
		isGuest: true,
		email: '',
		guestId: 7,
		id: '',
		name: '',
		logoutUser: function(){
			this.isLoggedIn = false;
			this.isGuest = false;
			$state.go('home.login');
		},
		loginUser: function(){
			this.isLoggedIn = true;
			this.isGuest = false;
			$state.go('home.main');
		},
		loginGuest: function(){
			this.isLoggedIn = true;
			this.isGuest = true;
			$state.go('home.main');
		},
		checkSession: function(){
			if(!this.isLoggedIn){
				$state.go('home.login');
			}
		}
		
	}
	
	return userService;
});

angular.module('ng').filter('cut', function () {
        return function (value, wordwise, max, tail) {
            if (!value) return '';

            max = parseInt(max, 10);
            if (!max) return value;
            if (value.length <= max) return value;

            value = value.substr(0, max);
            if (wordwise) {
                var lastspace = value.lastIndexOf(' ');
                if (lastspace != -1) {
                    value = value.substr(0, lastspace);
                }
            }

            return value + (tail || ' …');
        };
    });


