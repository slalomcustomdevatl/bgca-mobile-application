var app_api = "slabs.cc/bgca/";
var clubs_api = "bgca.cartodb.com/api/v2/sql";

app.factory('Club', function($resource){
	return $resource('mock/Clubs.json');
});

app.factory('Activity', function($resource){
	return $resource('mock/Activity.json', {path: app_api});
});

app.factory('Category', function($resource){
	return $resource('mock/Category.json', {path: app_api});
});

app.factory('Cost', function($resource){
	return $resource('mock/Cost.json', {path: app_api});
});

app.factory('Ethnicity', function($resource){
	return $resource('mock/Ethnicity.json', {path: app_api});
});

app.factory('Genre', function($resource){
	return $resource('mock/Genre.json', {path: app_api});
});

app.factory('OrgMetaData', function($resource){
	return $resource('http://:path/OrgMetaData', {path: app_api});
});

app.factory('Story', function($resource){
	return $resource('mock/Story.json', {path: app_api});
});

app.factory('User', function($resource){
	return $resource('http://:path/User', {path: app_api});
});
